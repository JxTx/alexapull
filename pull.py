#!/usr/bin/env python

import alexa
import sys

def pull(amount):
    sites = alexa.top_list(int(amount))
    f = open('sites.txt', 'w')
    for x in sites:
        print x[1]
        f.write(x[1]+'\n')
    f.close()

try:
    pull(sys.argv[1])
except IndexError:
    print '\n python %s <amount of sites>' % sys.argv[0]
    sys.exit()
